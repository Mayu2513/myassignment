# app
This app was made using: 
* Apache open nlp tools https://opennlp.apache.org/
* Jgrapht https://jgrapht.org/


### Installation
Clone this repository and import into Android Studio

### Run 
It can be run on an android studio virtual device or connected to an android studio device.

### About Code Format
- All UI related files should be in "ui" folder 
- Network and API related files should be in "rest" folder

### Why Choose MVVM Framework

The ViewModel acts as an interface between Model and View.
MVVM helps you achieve “Separation of Concerns” by guiding the structure and architecture of your code. Implementing MVVM demands a shift in mindset in how you think about your application's functioning. It has a steep learning curve and requires some extra effort up front to get started on the correct track.
- It's better to understand, maintain, and troubleshoot your code now. 
- Testability is possible thanks to your ViewModels. 
- Unit test coverage is possible, enabling for "Test-Driven Development" and "Automated Regressions."
- MVVM's ability to decouple the View from the ViewModel helps designers and developers to collaborate successfully.